package com.example.helloworld

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import java.time.LocalDateTime
import java.util.*

class DemoActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_demo)
    }

    fun clickHandler(view: View) {
        // static method
        val textView = findViewById<TextView>(R.id.show)
        textView.text = ("Hello Android-${Date()}")
    }
}
