package com.example.helloworld;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class HelloActivity extends AppCompatActivity {
    private static final String TAG = "HelloActivity";
    public static final String ACTION_START = "com.example.helloworld.ACTION_START";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hello);
        Log.d(TAG, "onCreate: Hello Java Activity.");
        Button button = findViewById(R.id.button_kotlin);
        button.setOnClickListener(v -> {
            Intent intent = new Intent(ACTION_START);
            startActivity(intent);
        });
    }
}
