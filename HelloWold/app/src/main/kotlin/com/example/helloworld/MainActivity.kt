package com.example.helloworld

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button

class MainActivity : AppCompatActivity() {
    companion object {
        internal const val TAG = "MainActivity"
    }
    private lateinit var buttonJava:Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        buttonJava = findViewById(R.id.button_java)
        buttonJava.setOnClickListener {
            startActivity(Intent(this@MainActivity, HelloActivity::class.java))
        }
        Log.d(TAG, "onCreate: 100% Android Girl Saga.")
    }
}
