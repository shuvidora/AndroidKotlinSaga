package com.example.activitylifecycle

import android.content.Intent
import android.nfc.Tag
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button

const val TAG = "MainActivity"
class MainActivity : AppCompatActivity() {
    private lateinit var buttonStartNormal: Button
    private lateinit var buttonStartDialog:Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d(TAG,"OnCreate")
        setContentView(R.layout.activity_main)
        val tempData = savedInstanceState?.getString("temp_data")
        Log.d(TAG, tempData.orEmpty())
        buttonStartNormal = findViewById(R.id.start_normal_activity)
        buttonStartDialog = findViewById(R.id.start_dialog_activity)
        buttonStartNormal.setOnClickListener {
            startActivity(Intent(this,NormalActivity::class.java))
        }
        buttonStartDialog.setOnClickListener {
            startActivity(Intent(this,DialogActivity::class.java))
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        val tempData = "Something you just typed"
        outState.putString("data_key",tempData)
        super.onSaveInstanceState(outState)
    }

    override fun onStart() {
        super.onStart()
        Log.d(TAG, "onStart")
    }

    override fun onPause() {
        super.onPause()
        Log.d(TAG, "onPause")
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG, "onResume")
    }

    override fun onStop() {
        super.onStop()
        Log.d(TAG, "onStop")
    }

    /**
     * Use adb to simulate a process shutdown
     */
    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "onDestroy")
    }

    override fun onRestart() {
        super.onRestart()
        Log.d(TAG, "onRestart")
    }
}
