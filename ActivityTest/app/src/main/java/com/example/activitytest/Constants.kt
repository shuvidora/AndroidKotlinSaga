package com.example.activitytest

class Constants {
    companion object {
        internal const val EXTRA_DATA_NAME = "extra_data"
        internal const val SEND_MSG:String = "Hello SecondActivity"
        internal const val DATA_RETURN_NAME:String = "Hello FirstActivity"
        internal const val DATA_RETURN_RESULT:String = "Hello FirstActivity"

        internal const val BAIDU_HTTP_DATA:String = "http://www.baidu.com"
        internal const val DIAL_TEL_DATA:String = "tel:10086"

        internal const val PICK_SECOND_REQUEST:Int = 1
    }
}