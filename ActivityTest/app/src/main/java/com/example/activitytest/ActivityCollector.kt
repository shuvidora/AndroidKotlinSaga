package com.example.activitytest

import android.app.Activity

class ActivityCollector {
    companion object {
        private val activities = mutableListOf<Activity>()

        fun addActivity(activity: Activity) {
            activities.add(activity)
        }
        fun removeActivity(activity: Activity) {
            activities.remove(activity)
        }

        fun finishAll() {
            activities.forEach {
                if (!it.isFinishing) {
                    it.finish()
                }
            }
            activities.clear()
        }
    }
}