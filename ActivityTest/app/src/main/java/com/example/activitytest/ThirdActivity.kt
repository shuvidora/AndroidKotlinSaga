package com.example.activitytest

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Button
import kotlinx.android.synthetic.main.third_layout.*

class ThirdActivity : BaseActivity() {
    companion object {
        internal const val TAG = "ThirdActivity"
    }
    private lateinit var webView:WebView
    private lateinit var button3: Button

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d(TAG, "Task id is $taskId")
        setContentView(R.layout.third_layout)

        val uri = intent.data.toString()
        webView = web_view
        webView.settings.javaScriptEnabled = true
        webView.webViewClient = WebViewClient()
        webView.loadUrl(uri)

        button3 = findViewById(R.id.button_3)
        button3.setOnClickListener {
            ActivityCollector.finishAll()
        }
    }
}
