package com.example.activitytest

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.first_layout.*

// Backwards Compatibility
class FirstActivity : BaseActivity() {
    companion object {
        internal const val TAG = "FirstActivity"
    }

    private lateinit var button1: Button
    private lateinit var buttonLaunch: Button
    private lateinit var buttonFinish: Button
    private lateinit var buttonExplicit: Button
    private lateinit var buttonImplicit: Button
    private lateinit var buttonBaidu: Button
    private lateinit var buttonSend: Button
    private lateinit var buttonResult: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d(TAG,"Task id is $taskId")
        setContentView(R.layout.first_layout)

        button1 = button_1
        button1.setOnClickListener {
            Toast.makeText(this@FirstActivity, R.string.click_msg, Toast.LENGTH_SHORT).show()
        }
        // launch mode
        buttonLaunch = button_launch
        buttonLaunch.text = String.format("hashCode:%h",this.hashCode())
        buttonLaunch.setOnClickListener {
//            startActivity(Intent(this,SecondActivity::class.java))
            startActivity<SecondActivity>()
        }

        buttonFinish = button_finish
        buttonFinish.setOnClickListener {
            finish()
        }

        // Intent
        buttonExplicit = button_explicit
        buttonExplicit.setOnClickListener {
//            startActivity(Intent(this@FirstActivity, SecondActivity::class.java))
            startActivity<SecondActivity>()
        }

        buttonImplicit = button_implicit
        buttonImplicit.setOnClickListener {
            val intent = Intent("com.example.activitytest.ACTION_START")
            intent.addCategory("com.example.activitytest.MY_CATEGORY")
            startActivity(intent)
        }

        buttonBaidu = button_baidu
        buttonBaidu.setOnClickListener {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(Constants.BAIDU_HTTP_DATA)).addCategory("com.example.activitytest.THIRD_CATEGORY"))
        }

        buttonSend = button_send
        buttonSend.setOnClickListener {
//            val intent = Intent(this@FirstActivity, SecondActivity::class.java)
//            intent.putExtra(Constants.EXTRA_DATA_NAME, Constants.SEND_MSG)
//            startActivity(intent)
            SecondActivity.actionStart(this@FirstActivity,"data1","data2")
        }

        buttonResult = button_result
        buttonResult.setOnClickListener {
            startActivityForResult(Intent(this@FirstActivity, SecondActivity::class.java), Constants.PICK_SECOND_REQUEST)
        }
    }

    override fun onRestart() {
        super.onRestart()
        Log.d(TAG, "onRestart")
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            Constants.PICK_SECOND_REQUEST -> {
                val bundle = intent.extras
                val returnedData = bundle?.getString(Constants.DATA_RETURN_NAME)
                Log.d("FirstActivity", returnedData.orEmpty())
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.add_item -> Toast.makeText(this, "You clicked Add", Toast.LENGTH_SHORT).show()
            R.id.remove_item -> Toast.makeText(this,"You clicked Remove",Toast.LENGTH_SHORT).show()
        }
        return true
    }


}
