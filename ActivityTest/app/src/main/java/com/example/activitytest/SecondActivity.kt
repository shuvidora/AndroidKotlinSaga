package com.example.activitytest

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import kotlinx.android.synthetic.main.second_layout.*

class SecondActivity : BaseActivity() {
    companion object{
        internal const val TAG = "SecondActivity"

        internal fun actionStart(context: Context, data1: CharSequence, data2: CharSequence) {
            val intent = Intent(context, SecondActivity::class.java)
            intent.putExtra("param1", data1)
            intent.putExtra("param2", data2)
            context.startActivity(intent)
        }
    }

    @BindView(R.id.button_2)
    lateinit var button2: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d(TAG, "Task id is $taskId")
        setContentView(R.layout.second_layout)
        ButterKnife.bind(this)

//        val data = intent.getStringExtra(Constants.EXTRA_DATA_NAME)
//        Log.d("SecondActivity",data.orEmpty())

        val param1 = intent.getStringExtra("param1")
        val param2 = intent.getStringExtra("param2")
        Log.d(TAG, "param1 = $param1, param2 = $param2")
    }

    @OnClick(R.id.button_2)
    internal fun jump() {
        startActivity(Intent(this, ThirdActivity::class.java))
//        handleIntent()
    }

//    override fun onDestroy() {
//        super.onDestroy()
//        Log.d(TAG, "onDestroy")
//    }

    override fun onBackPressed() {
        handleIntent()
    }

    private fun handleIntent() {
        val intent = Intent().apply {
            val bundle = Bundle()
            bundle.putString(Constants.DATA_RETURN_NAME, Constants.DATA_RETURN_RESULT)
            putExtras(bundle)
        }
        setResult(Activity.RESULT_OK, intent)
        finish()
    }
}
